package id.co.iconpln.tvseriesapp.model

object TvSeriesData {

    val listTvSeries: MutableList<TvSeries>
        get() {
            val list = mutableListOf<TvSeries>()
            tvSeriesData.forEach {
                list.add(it)
            }
            return list
        }

    private val tvSeriesData = arrayOf (
        TvSeries(
            "Breaking Bad",
            "A high school chemistry teacher diagnosed with inoperable lung cancer turns to manufacturing and selling methamphetamine in order to secure his family's future.",
            "2008–2013",
            5,
            9.5f,
            "https://m.media-amazon.com/images/M/MV5BMjhiMzgxZTctNDc1Ni00OTIxLTlhMTYtZTA3ZWFkODRkNmE2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,718,1000_AL_.jpg",
            "https://www.imdb.com/title/tt0903747/"
        ),
        TvSeries(
            "Black Mirror",
            "An anthology series exploring a twisted, high-tech multiverse where humanity's greatest innovations and darkest instincts collide.",
            "2011–",
            5,
            8.8f,
            "https://m.media-amazon.com/images/M/MV5BYTM3YWVhMDMtNjczMy00NGEyLWJhZDctYjNhMTRkNDE0ZTI1XkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_.jpg",
            "https://www.imdb.com/title/tt2085059/"
        ),
        TvSeries(
            "Rick and Morty",
            "An animated series that follows the exploits of a super scientist and his not-so-bright grandson.",
            "2013–",
            4,
            9.3f,
            "https://m.media-amazon.com/images/M/MV5BMjRiNDRhNGUtMzRkZi00MThlLTg0ZDMtNjc5YzFjYmFjMmM4XkEyXkFqcGdeQXVyNzQ1ODk3MTQ@._V1_.jpg",
            "https://www.imdb.com/title/tt2861424/"
        ),
        TvSeries(
            "How I Met Your Mother",
            "A father recounts to his children, through a series of flashbacks, the journey he and his four best friends took leading up to him meeting their mother.",
            "2005–2014",
            9,
            8.3f,
            "https://m.media-amazon.com/images/M/MV5BZWJjMDEzZjUtYWE1Yy00M2ZiLThlMmItODljNTAzODFiMzc2XkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_SY1000_CR0,0,666,1000_AL_.jpg",
            "https://www.imdb.com/title/tt0460649/"
        ),
        TvSeries(
            "The End of the F***ing World",
            "James is 17 and is pretty sure he is a psychopath. Alyssa, also 17, is the cool and moody new girl at school. The pair make a connection and she persuades him to embark on a road trip in search of her real father.",
            "2017–",
            2,
            8.1f,
            "https://m.media-amazon.com/images/M/MV5BN2ZhNmQ2MjQtMmQzMi00YjE5LTlkMWMtMjk5YzIxMjk2NDc2XkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SY1000_SX800_AL_.jpg",
            "https://www.imdb.com/title/tt6257970/"
        ),
        TvSeries(
            "Mr. Robot",
            "Elliot, a brilliant but highly unstable young cyber-security engineer and vigilante hacker, becomes a key figure in a complex game of global dominance when he and his shadowy allies try to take down the corrupt corporation he works for.",
            "2015–",
            4,
            8.5f,
            "https://m.media-amazon.com/images/M/MV5BMzgxMmQxZjQtNDdmMC00MjRlLTk1MDEtZDcwNTdmOTg0YzA2XkEyXkFqcGdeQXVyMzQ2MDI5NjU@._V1_SY1000_CR0,0,666,1000_AL_.jpg",
            "https://www.imdb.com/title/tt4158110/"
        ),
        TvSeries(
            "Silicon Valley",
            "Follows the struggle of Richard Hendricks, a Silicon Valley engineer trying to build his own company called Pied Piper.",
            "2014–",
            6,
            8.5f,
            "https://m.media-amazon.com/images/M/MV5BOTcwNzU2MGEtMzUzNC00MzMwLWJhZGItNDY3NDllYjU5YzAyXkEyXkFqcGdeQXVyMzQ2MDI5NjU@._V1_SY1000_SX675_AL_.jpg",
            "https://www.imdb.com/title/tt2575988/"
        ),
        TvSeries(
            "The Walking Dead",
            "Sheriff Deputy Rick Grimes wakes up from a coma to learn the world is in ruins, and must lead a group of survivors to stay alive.",
            "2010–",
            10,
            8.3f,
            "https://m.media-amazon.com/images/M/MV5BYWY4ODJiZjMtNWMxYi00ZmM5LWIwZmQtZWY0MjJmZGU5MjcxXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg",
            "https://www.imdb.com/title/tt1520211/"
        ),
        TvSeries(
            "The Mandalorian",
            "The travails of a lone bounty hunter in the outer reaches of the galaxy, far from the authority of the New Republic.",
            "2019–",
            1,
            9.1f,
            "https://m.media-amazon.com/images/M/MV5BMWI0OTJlYTItNzMwZi00YzRiLWJhMjItMWRlMDNhZjNiMzJkXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg",
            "https://www.imdb.com/title/tt8111088/"
        ),
        TvSeries(
            "Stranger Things",
            "When a young boy disappears, his mother, a police chief, and his friends must confront terrifying supernatural forces in order to get him back.",
            "2016–",
            3,
            8.8f,
            "https://m.media-amazon.com/images/M/MV5BZGExYjQzNTQtNGNhMi00YmY1LTlhY2MtMTRjODg3MjU4YTAyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg",
            "https://www.imdb.com/title/tt4574334/"
        ),
        TvSeries(
            "Sherlock",
            "A modern update finds the famous sleuth and his doctor partner solving crime in 21st century London.",
            "2010–",
            4,
            9.1f,
            "https://m.media-amazon.com/images/M/MV5BMWY3NTljMjEtYzRiMi00NWM2LTkzNjItZTVmZjE0MTdjMjJhL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNTQ4NTc5OTU@._V1_UX182_CR0,0,182,268_AL_.jpg",
            "https://www.imdb.com/title/tt1475582/"
        ),
        TvSeries(
            "True Detective",
            "Seasonal anthology series in which police investigations unearth the personal and professional secrets of those involved, both within and outside the law.",
            "2014–",
            3,
            9.0f,
            "https://m.media-amazon.com/images/M/MV5BMTUwMGM2ZmYtZGEyZC00OWQyLWI2Y2QtMTdjYzMxZGJmNjhjXkEyXkFqcGdeQXVyNjU2ODM5MjU@._V1_UX182_CR0,0,182,268_AL_.jpg",
            "https://www.imdb.com/title/tt2356777/"
        ),
        TvSeries(
            "Westworld",
            "Set at the intersection of the near future and the reimagined past, explore a world in which every human appetite can be indulged without consequence.",
            "2016–",
            2,
            8.8f,
            "https://m.media-amazon.com/images/M/MV5BNThjM2Y3MDUtYTIyNC00ZDliLWJlMmItNWY1N2E5NjhmMGM4XkEyXkFqcGdeQXVyNjU2ODM5MjU@._V1_UX182_CR0,0,182,268_AL_.jpg",
            "https://www.imdb.com/title/tt0475784/"
        ),
        TvSeries(
            "Chernobyl",
            "In April 1986, an explosion at the Chernobyl nuclear power plant in the Union of Soviet Socialist Republics becomes one of the world's worst man-made catastrophes.",
            "2019",
            1,
            9.5f,
            "https://m.media-amazon.com/images/M/MV5BNTEyYmIzMDUtNWMwNC00Y2Q1LWIyZTgtMGY1YzUxOTAwYTAwXkEyXkFqcGdeQXVyMjIyMTc0ODQ@._V1_UX182_CR0,0,182,268_AL_.jpg",
            "https://www.imdb.com/title/tt7366338/"
        )
    )
}