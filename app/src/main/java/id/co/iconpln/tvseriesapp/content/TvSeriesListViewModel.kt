package id.co.iconpln.tvseriesapp.content

import androidx.lifecycle.ViewModel
import id.co.iconpln.tvseriesapp.model.TvSeries
import id.co.iconpln.tvseriesapp.model.TvSeriesData

class TvSeriesListViewModel: ViewModel() {
    var listTvSeries: MutableList<TvSeries>

    init {
        listTvSeries = getTvSeriesList()
        listTvSeries.shuffle()
    }

    private fun getTvSeriesList(): MutableList<TvSeries> {
        return TvSeriesData.listTvSeries
    }
}
