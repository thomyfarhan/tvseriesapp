package id.co.iconpln.tvseriesapp.login

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import id.co.iconpln.tvseriesapp.R
import id.co.iconpln.tvseriesapp.checkEmailFormat
import id.co.iconpln.tvseriesapp.content.TvSeriesListActivity
import id.co.iconpln.tvseriesapp.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var username: String
    private lateinit var password: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        binding.btnLogin.setOnClickListener { loginAccount() }
    }

    private fun loginAccount() {
        if (checkField()) {
            if (checkAccount(username, password)) {
                startActivity(Intent(this, TvSeriesListActivity::class.java))
                finish()
            } else {
                Toast.makeText(this, "Email atau Password tidak sesuai", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun checkAccount(user: String, pass: String): Boolean {
        return user == "user@mail.com" && pass == "password"
    }

    private fun checkField(): Boolean {
        username = binding.etLoginUsername.text?.toString() ?: ""
        password = binding.etLoginPassword.text?.toString() ?: ""

        when {
            username.isBlank() && password.isBlank() -> {
                showWarningUsername("Username jangan Kosong!")
                showWarningPassword("Password jangan Kosong!")
            }
            username.isBlank() -> {
                showWarningUsername("Username jangan Kosong!")
            }
            password.isBlank() -> {
                showWarningPassword("Password jangan Kosong!")
            }
            !username.checkEmailFormat() -> {
                showWarningUsername("Format username tidak sesuai!")
            }
            password.length < 7 -> {
                showWarningPassword("Password harus lebih dari 7 karakter!")
            }
            else -> return true
        }
        return false
    }

    private fun showWarningPassword(text: String) {
        binding.etLoginPassword.error = text
        binding.etLoginPassword.requestFocus()
    }

    private fun showWarningUsername(text: String) {
        binding.etLoginUsername.error = text
        binding.etLoginUsername.requestFocus()
    }

    // This function allows to close keyboard if currentFocus changed
    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }
}
