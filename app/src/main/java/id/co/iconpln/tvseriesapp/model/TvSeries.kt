package id.co.iconpln.tvseriesapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TvSeries (
    val title: String = "",
    val synopsis: String = "",
    val year: String = "",
    val totalSeason: Int = 0,
    val rating: Float = 0f,
    val photo: String = "",
    val imdbPage: String = ""
): Parcelable