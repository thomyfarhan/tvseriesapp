package id.co.iconpln.tvseriesapp.content

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import id.co.iconpln.tvseriesapp.R
import id.co.iconpln.tvseriesapp.detail.TvSeriesDetailActivity
import id.co.iconpln.tvseriesapp.model.TvSeries
import kotlinx.android.synthetic.main.activity_tv_series_list.*

class TvSeriesListActivity : AppCompatActivity() {

    private lateinit var list: MutableList<TvSeries>
    private lateinit var viewModel: TvSeriesListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tv_series_list)

        initViewModel()
        loadAdapter()

        lvListSeries.setOnItemClickListener{ _, _, index, _ ->
            val intent = TvSeriesDetailActivity.newIntent(this, list[index])
            startActivity(intent)
        }
    }

    private fun loadAdapter() {
        list = viewModel.listTvSeries
        lvListSeries.adapter = ListViewTvSeriesAdapter(this, list)
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this).get(TvSeriesListViewModel::class.java)
    }
}
