package id.co.iconpln.tvseriesapp.detail

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.co.iconpln.tvseriesapp.R
import id.co.iconpln.tvseriesapp.model.TvSeries
import kotlinx.android.synthetic.main.activity_tv_series_detail.*
import java.text.DecimalFormat

class TvSeriesDetailActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_TVSERIES ="tvseries"

        fun newIntent(context: Context, tvSeries: TvSeries): Intent {
            val intent = Intent(context, TvSeriesDetailActivity::class.java)
            intent.putExtra(EXTRA_TVSERIES, tvSeries)

            return intent
        }
    }

    private lateinit var tvSeries: TvSeries

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tv_series_detail)

        tvSeries = getExtraTvSeries()

        setUpActionBar()
        displayDetail()

        btnTvSeriesImdbPage.setOnClickListener { doIntentView() }
    }

    private fun getExtraTvSeries(): TvSeries {
        return intent.getParcelableExtra(EXTRA_TVSERIES) ?: TvSeries()
    }

    private fun displayDetail() {
        tvTvSeriesDetailTitle.text = tvSeries.title
        rbTvSeriesDetailRating.rating = tvSeries.rating / 2
        tvTvSeriesDetailYears.text = tvSeries.year
        tvTvSeriesDetailSynopsis.text = tvSeries.synopsis
        Glide.with(this)
            .load(tvSeries.photo)
            .apply(
                RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_hourglass)
                    .error(R.drawable.ic_warning)
            )
            .into(ivTvSeriesDetailPhoto)
    }

    private fun setUpActionBar() {
        supportActionBar?.title = tvSeries.title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)
        if (getIntentShare().resolveActivity(packageManager) == null) {
            menu?.findItem(R.menu.menu_detail)?.isVisible = false
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.detail_share -> startActivity(getIntentShare())
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getIntentShare(): Intent {
        val intent = Intent(Intent.ACTION_SEND)
        val rating = DecimalFormat("0.#").format(tvSeries.rating)
        intent.putExtra(Intent.EXTRA_TEXT,
            getString(R.string.share_message, tvSeries.title, rating))
        intent.type = "text/plain"

        return Intent.createChooser(intent, "Pilih aplikasi yg menjadi tujuan")
    }

    private fun doIntentView() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(tvSeries.imdbPage))
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

}
