package id.co.iconpln.tvseriesapp

fun String.checkEmailFormat(): Boolean {
    return android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}
