package id.co.iconpln.tvseriesapp.content

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.co.iconpln.tvseriesapp.R
import id.co.iconpln.tvseriesapp.model.TvSeries

class ListViewTvSeriesAdapter(private val context: Context, private val listTvSeries: MutableList<TvSeries>) : BaseAdapter() {

    override fun getView(index: Int, view: View?, viewGroup: ViewGroup?): View {
        val viewLayout = LayoutInflater.from(context)
            .inflate(R.layout.item_list_tv_series, viewGroup, false)

        val viewHolder = ViewHolder(viewLayout)
        val tvSeries = getItem(index) as TvSeries
        viewHolder.bind(context, tvSeries)

        return viewLayout
    }

    override fun getItem(index: Int): Any {
        return listTvSeries[index]
    }

    override fun getItemId(index: Int): Long {
        return index.toLong()
    }

    override fun getCount(): Int {
        return listTvSeries.size
    }

    private inner class ViewHolder(view: View) {
        private val tvTvSeriesTitle: TextView = view.findViewById(R.id.tvTvSeriesTitle)
        private val tvTvSeriesYears: TextView = view.findViewById(R.id.tvTvSeriesYears)
        private val tvTvSeriesSynopsis: TextView = view.findViewById(R.id.tvTvSeriesSynopsis)
        private val tvTvSeriesSeason: TextView = view.findViewById(R.id.tvTvSeriesSeason)
        private val rbTvSeriesRating: RatingBar = view.findViewById(R.id.rbTvSeriesRating)
        private val ivTvSeriesPhoto: ImageView = view.findViewById(R.id.ivTvSeriesPhoto)

        fun bind(context: Context, tvSeries: TvSeries) {
            tvTvSeriesTitle.text = tvSeries.title
            tvTvSeriesYears.text = tvSeries.year
            tvTvSeriesSynopsis.text = tvSeries.synopsis
            tvTvSeriesSeason.text = context.getString(R.string.tv_adapter_season_text, tvSeries.totalSeason)
            rbTvSeriesRating.rating = tvSeries.rating/2

            Glide.with(context)
                .load(tvSeries.photo)
                .apply(
                    RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.ic_hourglass)
                        .error(R.drawable.ic_warning)
                )
                .into(ivTvSeriesPhoto)
        }
    }

}
